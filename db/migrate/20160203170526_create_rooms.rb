class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.string :name
      t.integer :number_members
      t.integer :timer, default: 60
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
