require 'test_helper'

class IndexTest < ActionDispatch::IntegrationTest
  test 'returns users in JSON' do
    get '/users.json', {}, {'Accept' => Mime::JSON}
    assert_equal 200, response.status
    assert_equal Mime::JSON, response.content_type
  end

  test 'returns rooms in JSON' do
    get '/rooms.json', {}, {'Accept' => Mime::JSON}
    assert_equal 200, response.status
    assert_equal Mime::JSON, response.content_type
  end
end