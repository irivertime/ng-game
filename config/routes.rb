Rails.application.routes.draw do
  mount JasmineRails::Engine => "/specs" if defined?(JasmineRails)
  devise_for :users
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  root to: 'application#index'

  resources :users do
    post :change_locale, on: :collection
  end

  resources :rooms do
    member do
      post :add_user
      post :remove_user
      post :start_timer
    end
  end
end
