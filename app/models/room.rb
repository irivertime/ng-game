class Room < ActiveRecord::Base
  include RoomPublisher

  has_many :room_users, :class_name => 'RoomUser', foreign_key: :room_id
  has_many :users, through: :room_users

  def as_json(options = {})
    super(options.merge(include: [:users]))
  end
end
