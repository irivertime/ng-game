class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  has_many :rooms, :class_name => 'Room'

  attr_reader :online

  validates :username, presence: true, length: { maximum: 50 }, uniqueness: true

  def as_json(options = {})
    super(options.merge(include: [:rooms, :online]))
  end
end
