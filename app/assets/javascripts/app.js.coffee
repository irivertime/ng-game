angular.module('ngGame', ['ui.router', 'templates', 'pascalprecht.translate', 'faye', 'Devise', 'ui.bootstrap'])
.config([
  '$stateProvider', '$urlRouterProvider', '$translateProvider',
  ($stateProvider, $urlRouterProvider, $translateProvider)->
    locale = I18n.currentLocale()
    _.each I18n.translations, (num, key) ->
      $translateProvider.translations(key, I18n.translations[key])

    $translateProvider.preferredLanguage(locale)
    $translateProvider.useSanitizeValueStrategy('escape')

    $stateProvider
    .state('index', {
      url: '/',
      templateUrl: 'games/_index.html',
      controller: 'GameCtrl',
      resolve: {
        usersPromise:['games', 'Auth', '$stateParams', (games, Auth, $stateParams) ->
          return games.getAllUsers()
        ],
        roomsPromise:['games', 'Auth', '$stateParams', (games, Auth, $stateParams) ->
          return games.getAllRooms()
        ]
      }
    })
    .state('rooms', {
      url: '/room/:roomId',
      templateUrl: 'rooms/_index.html',
      controller: 'RoomCtrl',
      resolve: {
        roomPromise: ['rooms', 'Auth', '$stateParams', (rooms, Auth, $stateParams) ->
          return rooms.getById($stateParams.roomId)
        ]
      }
    })
    .state('login', {
      url: '/login',
      templateUrl: 'auth/_login.html'
      controller: 'AuthCtrl'
    })
    .state('register', {
      url: '/register'
      templateUrl: 'auth/_register.html'
      controller: 'AuthCtrl'
      onEnter: ['$state', 'Auth', ($state, Auth) ->
        Auth.currentUser().then( ()->
          $state.go('index')
        )
      ]
    })
    $urlRouterProvider.otherwise('/')
])
.factory('Faye', ['$faye', ($faye) ->
  $faye("/faye")
])