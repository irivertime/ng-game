angular.module('ngGame')
.directive('flash', ()->
  return {
    templateUrl: 'directives/_flash_notice.html'
  }
)