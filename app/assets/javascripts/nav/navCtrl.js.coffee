angular.module('ngGame')
.controller('NavCtrl', [
  '$translate', '$scope', 'Auth', 'Faye'
  ($translate, $scope, Auth, Faye) ->
    $scope.signedIn = Auth.isAuthenticated
    $scope.logout = Auth.logout

    Auth.currentUser().then( (user)->
      $scope.user = user

      Faye.client.addExtension outgoing: (message, callback) ->
        if message.channel == '/meta/subscribe' || message.channel == '/meta/disconnect'
          message.user = message.user || {}
          message.user.id = user.id
        callback message
    )
    $scope.$on('devise:new-registration', (e, user)->
      $scope.user = user
    )
    $scope.$on('devise:login', (e, user)->
      $scope.user = user
    )
    $scope.$on('devise:logout', (e, user)->
      $scope.user = {}
    )
    $scope.changeLanguage = (lang)->
      $translate.use(lang)
])