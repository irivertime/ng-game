#= require angular
#= require angular-rails-templates
#= require angular-ui-router
#= require jquery
#= require jquery_ujs
#= require bootstrap/dist/js/bootstrap
#= require angular-devise
#= require angular-bootstrap
#= require underscore
#= require angular-faye
#= require angular-translate
#= require faye
#= require angular-timer
#= require humanize-duration
#= require momentjs
#= require i18n
#= require i18n/translations

#= require_tree .
