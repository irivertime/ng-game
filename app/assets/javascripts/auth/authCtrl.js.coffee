angular.module('ngGame')
.controller('AuthCtrl', [
  '$translate', '$scope', '$state', 'Auth'
  ($translate, $scope, $state, Auth) ->
    $scope.login = ->
      Auth.login($scope.getUserWithLocale()).then ((user)->
        $state.go('index')
      ), (error) ->
        $scope.auth_notices =  [{type: 'danger', title: error.data.error}]
    $scope.register = ()->
      Auth.register($scope.getUserWithLocale()).then ((user)->
        $state.go('index')
      ), (error) ->
        $scope.auth_notices = []
        _.each _.flatten(_.values(error.data.errors)), (value, key) ->
          $scope.auth_notices.push({type: 'danger', title: value})


    $scope.getUserWithLocale = ()->
      user = $scope.user || {}
      user.locale = $translate.use()
      return user
])
