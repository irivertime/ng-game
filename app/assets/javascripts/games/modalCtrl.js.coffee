angular.module('ngGame')
.controller('ModalCtrl', [
  '$scope', '$modal'
  ($scope, $modal)->
    $scope.openModalRoom = ()->
      $scope.modalInstance = $modal.open
        animation: true,
        scope: $scope,
        templateUrl: 'games/_room_modal.html'

    $scope.cancel =  ()->
      $scope.modalInstance.dismiss('cancel')

])