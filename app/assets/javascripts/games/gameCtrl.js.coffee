angular.module('ngGame')
.controller('GameCtrl', [
  '$scope', '$state', '$compile', '$controller', '$document', 'games', 'rooms', 'Auth', 'Faye'
  ($scope, $state, $compile, $controller, $document, games, rooms, Auth, Faye)->
    angular.extend(this, $controller('ModalCtrl', {$scope: $scope}));
    $scope.signedIn = Auth.isAuthenticated
    $scope.blockAction = false
    $scope.timerWait = 60
    $scope.users = games.users
    $scope.rooms = games.rooms
    $scope.formRoom = {}

    Auth.currentUser().then( (user)->
      $scope.user = user

      Faye.subscribe "/channel_user_#{user.id}", (data) ->
        if data.status == 'block_action'
          $scope.blockAction = true
          $scope.timerWait = data.timer
        if data.status == 'access_action'
          $scope.blockAction = false
    )

    $scope.createNewRoom = ()->
      $scope.flashes = []
      $scope.openModalRoom()

    $scope.submitRoom = ->
      if not ($scope.formRoom.name and $scope.formRoom.number_members)
        $scope.flashes =  [{type: 'danger', title: 'empty_fields'}]
        return
      rooms.create($scope.formRoom)
      $scope.flashes =  [{type: 'success', title: 'success_save'}]
      $scope.cancel()

    $scope.current_user = ()->
      return ($scope.user || {}).id

    $scope.currectUser = (user_id)->
      return $scope.current_user() == user_id

    $scope.currectNumberMembers = (room)->
      return room.users.length < room.number_members

    $scope.isMember = (room)->
      for u in room.users
        if u.id == $scope.current_user()
          return true
      return false

    $scope.owener = (room)->
      return room.user_id == $scope.current_user()

    $scope.activeGame = (room)->
      return $scope.currectNumberMembers(room) && !$scope.isMember(room) && !$scope.blockAction

    $scope.addUser = (room_id)->
      rooms.addUser(room_id)
      $state.go('rooms', {roomId: room_id})

    Faye.subscribe "/users_online", (data) ->
      for su in $scope.users
        su.online = _.contains(data.users, su.id)

    Faye.subscribe "/newsRoom", (data) ->
      if data.status == 'add_user'
        for room in $scope.rooms
          if room.id == data.room.id
            room.users = data.room.users

      if data.status == 'remove_user'
        for room in $scope.rooms
          if room.id == data.room.id
            room.users = data.room.users
            if _.isEmpty(room.users)
              $scope.rooms = _.reject($scope.rooms, (r) ->
                r.id == room.id
              )

      if data.status == 'add_room'
        added = false
        for room in $scope.rooms
          if room.id == data.room.id
            added = true
        if !added
          $scope.rooms.push(data.room)

      if data.status == 'start' || data.status == 'process'
        for room in $scope.rooms
          if room.id == data.room.id
            room.goes = true

      if data.status == 'stop'
        for room in $scope.rooms
          if room.id == data.room.id
            room.goes = false

])