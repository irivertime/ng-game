angular.module('ngGame')
.factory('games', [
  '$http', '$state'
  ($http, $state)->
    o = {
      users: [],
      rooms: []
    }

    o.getAllUsers = ()->
      return $http.get('/users.json').success (data)->
        angular.copy(data, o.users)

    o.getAllRooms = ()->
      return $http.get('/rooms.json').success (data)->
        angular.copy(data, o.rooms)

    return o
])