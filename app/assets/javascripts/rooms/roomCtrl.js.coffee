angular.module('ngGame')
.controller('RoomCtrl', [
  '$scope', '$state', '$compile', '$document', '$interval', 'rooms','Auth', 'Faye'
  ($scope, $state, $compile, $document, $interval, rooms, Auth, Faye)->
    $scope.signedIn = Auth.isAuthenticated
    $scope.room = rooms.room
    $scope.timer_sec = $scope.room.timer
    $scope.started = false
    $scope.finished = false

    Auth.currentUser().then( (user)->
      $scope.user = user
    )

    $scope.current_user = ()->
      return ($scope.user || {}).id

    $scope.owener = ()->
      return $scope.room.user_id == $scope.current_user()

    $scope.currectNumberMembers = ()->
      return $scope.room.users.length == $scope.room.number_members

    $scope.isMember = ()->
      for u in $scope.room.users
        if u.id == $scope.current_user()
          return true
      return false

    $scope.addUser = ()->
      rooms.addUser()

    $scope.removeUser = ()->
      rooms.removeUser($scope.started)
      $state.go('index')

    $scope.startRound = ()->
      rooms.startTimer()

    Faye.subscribe "/newsRoom", (data) ->
      if data.room.id == $scope.room.id
        if data.status == 'add_user'
          $scope.room = data.room

        if data.status == 'remove_user'
          $scope.room = data.room

        if data.status == 'start'
          $scope.finished = false
          $scope.started = true

        if data.status == 'process'
          $scope.timer_sec = data.timer

        if data.status == 'stop'
          $scope.started = false
          $scope.finished = true

])