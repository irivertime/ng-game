angular.module('ngGame')
.factory('rooms', [
  '$http', '$state'
  ($http, $state)->
    o = {
      room: []
    }

    o.getById = (id)->
      return $http.get("/rooms/#{id}.json").success (data)->
        angular.copy(data, o.room)

    o.create = (room)->
      return $http.post('/rooms.json', room).success (data)->
        $state.go('rooms', {roomId: data.id})

    o.addUser = (room_id)->
      return $http.post("/rooms/#{room_id}/add_user.json").success (data)->

    o.removeUser = (status) ->
      return $http.post("/rooms/#{o.room.id}/remove_user.json", {status: status}).success (data)->

    o.startTimer = () ->
      return $http.post("/rooms/#{o.room.id}/start_timer.json").success (data)->

    return o
])