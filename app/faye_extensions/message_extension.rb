class MessageExtension
  MONITORED_CHANNELS = ['/meta/subscribe', '/meta/disconnect']
  def incoming(message, _request, callback)
    if MONITORED_CHANNELS.include? message['channel']
      StreamerController.check_users(message['channel'], message['user'])
    end
    callback.call(message)
  end
end