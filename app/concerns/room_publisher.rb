require 'thread'

module RoomPublisher
  extend ActiveSupport::Concern

  def publish(h = {})
    channel = h[:channel] || '/newsRoom'
    status = h[:status]
    message = h[:message] || { status: status, room: self }

    StreamerController.publish(channel, message)
  end


  def start_timer
    publish({ status: 'stop' })

    Thread.new do
      publish({ status: 'start' })
      timer = self.timer || 60
      timer.downto 0  do |i|
        publish({ message: { status: 'process', room: self, timer: i } })
        sleep 1
      end
      publish({ status: 'stop' })
    end
  end

  def block_action(user)
    Thread.new do
      timer = 60
      timer.downto 0 do |i|
        publish({ channel: "/channel_user_#{user.id}", message: { status: 'block_action', timer: i } })
        sleep 1
      end
      publish({ channel: "/channel_user_#{user.id}", message: { status: 'access_action' } })
    end
  end

end