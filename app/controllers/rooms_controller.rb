class RoomsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_room, only: [:show, :add_user, :remove_user, :start_timer]

  def index
    respond_with Room.all
  end

  def show
    respond_with @room
  end

  def add_user
    @room.room_users.build(user_id: current_user.id).save!
    @room.publish({ status: 'add_user' })

    render json: @room.to_json
  end

  def remove_user
    status = params[:status]
    @room.room_users.where(user_id: current_user.id).first.destroy!

    @room.publish({ status: 'remove_user' })
    @room.block_action(current_user) if status
    @room.destroy! if @room.room_users.empty?

    render json: @room.to_json
  end

  def create
    @room = Room.create(posts_params.merge(user_id: current_user.id))
    @room.room_users.build(user_id: current_user.id).save!
    @room.publish({ status: 'add_room' })

    render json: @room.to_json
  end

  def start_timer
    @room.start_timer

    render json: {}, status: 200
  end

  private

  def posts_params
    params.permit(:name, :number_members)
  end

  def set_room
    @room = Room.find params[:id]
  rescue ActiveRecord::RecordNotFound => e
    render json: { error: e.message }, status: :not_found
  end
end
