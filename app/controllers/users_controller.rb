class UsersController < ApplicationController
  before_action :authenticate_user!

  def index
    active_users = StreamerController::ACTIVE_USERS
    users = User.where.not(id: current_user.id)
    users_all = []
    users.each do |user|
      user_a = user.as_json
      user_a['online'] = active_users.include?(user.id)
      users_all << user_a
    end
    respond_with users_all
  end
end
