class StreamerController < FayeRails::Controller

  ACTIVE_USERS = []

  # channel '/widgets' do
  #   subscribe do
  #     puts "Received on channel #{channel}: #{message.inspect}"
  #   end
  # end

  def self.check_users(channel, user)
    return unless user
    if channel == '/meta/subscribe'
      unless ACTIVE_USERS.include? user['id']
        ACTIVE_USERS.push(user['id'])
        publish('/users_online', { status: 'online', users: ACTIVE_USERS })
      end
    elsif channel == '/meta/disconnect'
      ACTIVE_USERS.delete(user['id'])
      publish('/users_online', { status: 'online', users: ACTIVE_USERS })
    end
  end

end